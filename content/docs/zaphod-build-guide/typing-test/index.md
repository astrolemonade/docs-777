+++
title = "Typing Test"
description = "Time to take Zaphod for a drive.."
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 55
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "With Zaphod fully assembled, it's time to test it out!"
toc = true
top = false
images = ["layout.png"]
+++

## Default Keymap

Zaphod ships with the following default keymap:

[![Default Layout](layout.png)](http://www.keyboard-layout-editor.com/#/gists/c921f16f09124939db46fc301bf06096)

It is loosely based on [Miryoku](https://github.com/manna-harbour/miryoku), with some changes to account for
Zaphod having two thumbs, not three, on each side.

### Layers

Due to the reduced keycount on Zaphod, [layers](https://zmkfirmware.dev/docs/features/keymaps#layers) are heavily used to make the most of a small keyboard. In particular, all four thumbs on Zaphod are used as [layer taps](https://zmkfirmware.dev/docs/behaviors/layers#layer-tap). The layer activated by holding a thumb key is noted on the bottom edge of the keys in the diagram.

#### Navigation Layer

The navigation layer is activated by holding the leftmost thumb key. This gains access to arrow keys, home/end, page up/down, etc. This layer also gives you easy access to an Escape and Delete keys on the opposing thumb keys.

#### Media Layer

The media layer is activating by holding the inner thumb key on the left hand. Right now, it is largely used to control bluetooth profile selection, and clear the current bluetooth profile.

To learn more, see the [ZMK Bluetooth Behavior Docs](https://zmkfirmware.dev/docs/behaviors/bluetooth).

#### Number Layer

The number layer is activating by holding the inner thumb key on the right hand. It is used to access the number keys, as well as some additional symbols on the left hand.

#### Symbol Layer

The symbol layer is activating by holding the rightmost thumb key. It is used to access the symbol keys on the left hand, to avoid needing to *also* hold a shift key on the number layer.

#### Function Layer

To account for the fewer thumbs, the function layer for Zaphod can be reached by using a [combo](https://zmkfirmware.dev/docs/features/combos),
by pressing both right thumb keys at the same time.

This allows using the F1 through F12 keys, print screen, etc.

## USB

With Zaphod plugged in, you should be able to start typing and verify that the keyboard is detected and working properly.

## Wireless

ZMK will automatically start advertising to allow new hosts to connect. From your computer, go to the bluetooth preferences and add a device. You should see a device named "Zaphod" in the list. Click that item to connect. Once connected over bluetooth, you should be able to now unplug Zaphod from USB, and start typing over BLE.

