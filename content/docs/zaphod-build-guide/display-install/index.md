+++
title = "Display"
description = "Steps to install the Adafruit Sharp Memory Display."
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 15
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Zaphod is designed to work with the <a href=\"https://www.adafruit.com/product/3502\">Adafruit Sharp Memory Display</a>. The Zaphod display should be installed first, before any switch or case assembly. "
toc = true
top = false
images = ["test.jpg"]
+++

## Kit Contents

![Kit Contents](kit-content.jpg)

The display kit you purchased with your Zaphod includes the following items:

|Qty|Name|Sources|
|---|---|---|
|1|Adafruit Sharp Memory Display|[adafruit](https://www.adafruit.com/product/3502), [mouser](https://www.mouser.com/ProductDetail/Adafruit/3502?qs=HXFqYaX1Q2xCJzdR1%252B6HxQ%3D%3D)|
|1|1 x 5 Mill-Max Spring Loaded Header (825-22-005-10-001101) a.k.a. Pogo pins|[mouser](https://www.mouser.com/ProductDetail/Mill-Max/825-22-005-10-001101?qs=%2Fha2pyFaduhRND9PgH3elQPHltqScnFXbuAe7gILC7qwbao%252BU2ncDUpRCFH9HVFD), [digikey](https://www.digikey.com/en/products/detail/mill-max-manufacturing-corp/825-22-005-10-001101/6149696)|
|4|m2.5 x 6mm male-female nylon standoff|[aliexpress](https://www.aliexpress.com/item/M2-M2-5-M3-M4-Black-White-Hex-Nylon-Standoff-Male-Female-Plastic-Mount-Hexagon-Thread/32871403400.html?spm=a2g0s.9042311.0.0.78d74c4dlA60K0)|
|4|m2.5 nylon nut|[aliexpress](https://www.aliexpress.com/item/White-Black-Transparent-Nylon-Hex-Metric-Nut-Hexagon-Threaded-Plastic-Nuts-M2-M2-5-M3-M4/32868992270.html?spm=a2g0s.9042311.0.0.78d74c4dlA60K0)|
|4|m2.5 x 6mm philips nylon pan head screws|[aliexpress](https://www.aliexpress.com/item/M2-M2-5-M3-M4-M5-M6-M8-Black-White-Nylon-Round-Pan-Head-Machine-Screw/32870030598.html?spm=a2g0s.9042311.0.0.78d74c4dlA60K0)|

If you did not purchase the display kit with your Zaphod PCB, the parts can be purchased separately. The links provided are some options, but anything matching those specs will work.

<aside>
Note: spring-loaded headers are not strictly required, but using standard pin headers require soldering pins to both the main PCB and the display PCB, which will make any future maintenance much more difficult. This guide assumes you are using the spring-loaded headers for the electrical connection to the display; if using standard pin headers, adjust the installation accordingly.
</aside>

## Installation

First, attach the standoffs to the four mounting holes of the Zaphod PCB:

1. Place a screw up through the bottom of the PCB
1. Thread the female end of the standoff onto the screw
1. Repeat for all four standoffs

![Standoff Assembly](standoffs.jpg)

Next, insert the spring-loaded header into the Zaphod where indicated by the "Display Pins" label. Be **sure** to use the pins labeled "Display Pins" on the front silk. The extra pins are there
for possible future alternative modules used with Zaphod. When correctly place, you will see three empty holes to the left of the spring-loaded header, and one empty hole to the right.

Note: the "springy" part of the spring-loaded header should be up, with the solid pins inserted into the Zaphod PCB.

![Spring-Loaded Header Placement](pogos.jpg)

Before you solder the spring-loaded header to the Zaphod board, attach the display. This step will ensure the spring-loaded header is properly aligned during soldering:

1. Gently place the display so the four holes on the corners slide over the male end of the standoff
1. Make sure the spring-loaded header tips are properly aligned with the pin holes on the bottom of the display PCB
1. Add four nuts on each mounting post, slowly tightening each corner to engage the spring-loaded header, and ensure everything is in place.

![Display Install](display-installed.jpg)

**It is important that you verify the spring-loaded header alignment before soldering!**

![Spring-Loaded Header Alignment](pogo-alignment.jpg)

Once the display is fully tightened, flip over Zaphod and gently place the keyboard face down on a soft material that will protect the display.

Now you can solder the five pins of the spring-loaded header from the bottom.

**Do NOT solder the spring-loaded header to the display PCB; these pins are designed to make a stable connection to the display without requiring soldering.**

![Soldered Spring-Loaded Header](pogos-soldered.jpg)

## Testing

You should now be able to plug in a USB cable to keyboard and verify the display is working:

![Display Test](test.jpg)

## Next Steps

With the spring-loaded header properly soldered to the Zaphod PCB, unscrew the nuts and set the nuts and display aside until the rest of the assembly is complete. This will help ensure the display is not harmed during the remaining assembly steps.