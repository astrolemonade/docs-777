+++
title = "Batteries"
description = "Installing lithium-polymer (LiPo) batteries for Zaphod"
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Installing lithium-polymer (LiPo) batteries for Zaphod"
toc = true
top = false
images = ["battery-installed.jpg"]

+++

## Overview

Zaphod has charging circuitry to properly and safely charge batteries and is designed to work with 3.7v lithium-polymer (LiPo) batteries. Zaphod will charge batteries at 100mA when power is supplied via the USB port and will stop charging when the battery is fully charged.

## Specs

The following specifications will help ensure you select an appropriate battery to use with Zaphod. 

### Size/Dimensions

The following dimensions are for a battery that can be tucked beneath the display on your Zaphod (or where your display would go if you aren't using one).

<dl>
  <dt>Height</dt>
  <dd>7.3mm</dd>
  <dt>Length</dt>
  <dd>28.5mm</dd>
  <dt>Width</dt>
  <dd>20mm</dd>
</dl>

### Connector

<aside>Always take the proper precautions when handling the LiPo battery. Failure to do so can cause permanent damage to the board or yourself or start a fire. 

* Check the battery for damage, leaks, or swelling -- do NOT use the battery if any issues are found.
* Check the orientation of the wires, e.g., red and black.
* Do NOT allow the exposed ends of the red and black wires to touch.
</aside>

The Zaphod includes a JST PH-2.0 socket to make connecting and disconnecting batteries easy. When the JST plug is oriented with the alignment ridge on top of the connector and the wires extending away from you, as pictured, then the black wire should be on the right, and the red wire should be on the left.

{{ image(src="jst-ph-2.0.jpg", alt="JST PH-2.0 Plug")}}


If you purchase a battery that does not have a connector, there are pads on Zaphod to allow you to solder the battery directly. 

## Purchasing

<!-- Due legal and shipping constraints, batteries may not be available to be added to your kit. -->

The ideal LiPo battery suitable for Zaphod that will fit under the display properly is the `702025` LiPo, which should have a 300 mAh capacity. If purchasing a battery separately, the following are recommended:

* Dimensions for the battery to fit below the display: 7.3mm x 28.5mm x 20mm.
* JST socket to allow you to easily plug and unplug the battery.

If your battery does not have a JST socket, Zaphod also includes pads for soldering the battery connections directly onto the PCB.


## Installation

Installation steps will vary, depending on the battery connectors.

### JST Connector

If you have purchased a battery with a JST connector, first double check that the red and black wires are on the correct side. There is a silk label in front of the JST socket on Zaphod to remind you which wire should be on which side when inserting the plug.

Gently insert the JST plug into the socket. It's suggested to hold the socket in place on the PCB when doing so, to be sure the socket is secure.

Once connected, place the battery pack into the battery cutout slot in the PCB

{{ image(src="battery-installed.jpg",alt="Installed Battery") }}

### Bare Wires

If you are soldering a bare wire battery to Zaphod, *do not allow the exposed ends of the red and black wires to touch as this may cause the battery to explode.** Most batteries without a JST socket will come with the positive red wire covered in tape. Be sure to leave that tape on until you are ready to solder that wire.

1. Solder the black wire to the pin on the Zaphod PCB labeled "Black -". It should the right pin when looking down on the top of the Zaphod when it is oriented like normal.
2. Check to ensure there is no short from the black wire/pad to the red/positive pad next to it on the Zaphod PCB. 
3. Remove the tape from the red positive lead of the battery, being careful to prevent the red wire from touching the ground or any other part of the Zaphod PCB connections.
4. Carefully solder the red positive battery wire to the pin on Zaphod labeled "Red +".

## Status LED

Zaphod includes an orange LED that is used to indicate various charging information to users.

<dl>
  <dt>Off</dt>
  <dd>Battery is discharging, or no USB power is being supplied to charge the battery.</dd>
  <dt>Blinking</dt>
  <dd>USB power supplied, but no battery connected/detected.</dd>
  <dt>Solid (Bright)</dt>
  <dd>USB power supplied, and the battery is actively being charged.</dd>
  <dt>Solid (Dim)</dt>
  <dd>USB power supplied, but the battery is fully charged.</dd>
</dl>
