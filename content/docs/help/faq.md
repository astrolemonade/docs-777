+++
title = "FAQ"
description = "Answers to frequently asked questions."
date = 2021-05-01T19:30:00+00:00
updated = 2021-05-01T19:30:00+00:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## Is there a GUI keymap editor?

ZMK doesn't yet have one, but it is planned. Once it is available, Zaphod support will be added.

## How can I get help?

Contact [support](mailto:support@lpgala.xyz) or join the [Discord server](https://discord.gg/qUVC3zmf).