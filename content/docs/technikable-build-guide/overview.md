+++
title = "Overview"
description = "Technikable, a low profile, ortholinear, wireless keyboard"
date = 2022-08-04T08:00:00+00:00
updated = 2022-08-04T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Technikable is a low profile, ortholinear, wireless keyboard. This guide will walk you through assembling it."
toc = false
top = false
+++

{{ image(src="technikable.jpg", alt="Technikable Keyboard")}}

## Resources

If you are new to ZMK, check the [ZMK docs](https://zmk.dev/docs) and join the [Discord server](https://zmk.dev/community/discord/invite).

Need help with anything? Hop on the `#technik-beiwagon` channel on the [BoardSource Discord](https://discord.gg/dWCnE3zBm3) and ask away!
