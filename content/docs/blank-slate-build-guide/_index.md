+++
title = "Blank Slate"
description = "Build guide for assembling the Blank Slate keyboard"
date = 2022-08-04T08:00:00+00:00
updated = 2022-08-04T08:00:00+00:00
template = "docs/section.html"
sort_by = "weight"
weight = 1
draft = false
+++
