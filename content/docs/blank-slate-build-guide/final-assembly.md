+++
title = "Final Assembly"
description = "Final assembly into the case, and adding switches"
date = 2022-08-04T08:00:00+00:00
updated = 2022-08-04T08:00:00+00:00
draft = false
weight = 45
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Securing the PCB in the case, and adding keycaps will complete the assembly"
toc = false
top = false
+++

## Power Switch

Before finishing assembly, be sure to double check the power switch on the bottom side of the Blank Slate PCB is
switched to the "On" position.

## Case Assembly

The specifics of the case assembly will vary by case. For tray mount cases like the Drop Planck case, the only steps required are gently sliding the PCB, USB port first, into the case, and then inserting and tightening the screws through the bottom of the case into the PCB's built-in standoffs.

## Keycaps

Once the assembly is secured, add your keycaps to the switches:

{{ image(src="keycaps.jpg", alt="Keycaps Added")}}
