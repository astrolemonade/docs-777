+++
title = "Overview"
description = "Blank Slate, an ortholinear, wireless keyboard"
date = 2023-08-01T08:00:00+00:00
updated = 2023-08-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Blank Slate is a Planck-case compatible, ortholinear, wireless keyboard. This guide will walk you through assembling it."
toc = false
top = false
+++

{{ image(src="blank-slate.jpg", alt="Blank Slate Keyboard")}}

## Preface

This guide will cover aspects of the Blank Slate assembly and usage that are unique to this particular PCB.
Due to the nature of Blank Slate's compatibility with a variety of cases, it's recommended to read through
both this assembly guide, and any guides available for your particular case before continuing, to be sure
you assemble your keyboard in the correct order and without mistake.

## Resources

If you are new to ZMK, check the [ZMK docs](https://zmk.dev/docs) and join the [Discord server](https://zmk.dev/community/discord/invite).

Need help with anything? Hop on the `#blank-slate` channel on the [LP Galaxy Discord](https://discord.gg/f62w5f9Sdz) and ask away!
