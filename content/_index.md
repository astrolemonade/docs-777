+++
title = "LP Galaxy"

[extra]
lead = "Low profile keyboards for the discerning space traveler"
url = "/docs/"
url_button = "Build Guides"
repo_license = "Commercial"
repo_version = "GitHub"
repo_url = "https://gitlab.com/lpgalaxy/web/docs"


[[extra.list]]
title = "Zaphod"
content = "34-key wireless un-split for on the road efficiency."


[[extra.list]]
title = "Technikable"
content = "A low profile, wireless, ortholinear keyboard."

+++
